/*
*************************************************
Membre du groupe :
 => Moulahcene Djouher , NumEtu : 22014995
 => Ouamrane Lydia , NumEtu : 22115821
 => Arhainx Guillaume , NumEtu : 21911673
 => Poggi Sasha , NumEtu : 21904957
*/
/*
*************************************************
Requêtes de tests des triggers / fonctions / procédures
*/

/* ==> Test du trigger "controle_numeroTel" avec des numeros de tel inférieur à 10 */

INSERT INTO VOYAGEUR  (NUM_VOYAGEUR,NOM,PRENOM,TEL,DATE_NAI) VALUES  ('99999','Fournier','Lydi','0785','08/02/2000');
UPDATE VOYAGEUR SET TEL = '88927' WHERE NUM_VOYAGEUR = '17548';

/* ==> Test du trigger "GareDepArr" dans différents cas */

INSERT INTO VOYAGE VALUES  ('67632','10478','10457','10457','18/02/2021 17:30:00');
UPDATE VOYAGE SET ID_GARE_Dep = '10545' WHERE NUM_TRAINS = '10000' AND NUM_VOYAGEUR = '17892';
UPDATE VOYAGE SET ID_GARE_Arr = '10074' WHERE NUM_TRAINS = '10000' AND NUM_VOYAGEUR = '17892';

SELECT O.NUM_OBJET, NATURE, ETAT_OBJET FROM OBJET O, FAIT_UNE_DECLARATION_O F
 WHERE F.DATE_D = '17/02/2021'
 AND NUM_VOYAGEUR = '17632'
 AND O.NUM_OBJET = F.NUM_OBJET;

EXECUTE obj_trouve('17632','17/02/2021');

SELECT O.NUM_OBJET, NATURE, ETAT_OBJET FROM OBJET O, FAIT_UNE_DECLARATION_O F
 WHERE F.DATE_D = '17/02/2021'
 AND NUM_VOYAGEUR = '17632'
 AND O.NUM_OBJET = F.NUM_OBJET;


 SELECT num_obj_t('GUITARE') FROM DUAL;


